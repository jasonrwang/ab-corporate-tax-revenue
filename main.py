# %%

import pandas as pd
import altair as alt

# %%
# Load the data
df = pd.read_excel('https://open.alberta.ca/dataset/ca44381a-5392-48cf-a659-9f2e2eea7ae8/resource/0fe93035-4b6b-44f5-b7f5-7879b12a6b0a/download/goadesktope_jjacques.lorddesktopcit-naics-historical-open-data.xlsx',skiprows=2)
df = df.iloc[:,1:]
df.head()

# %%
# Format the data
df = df.melt(id_vars='Industry Group', var_name='Year', value_name='Revenue').dropna()

# %%
# Build visualization
base = alt.Chart(df).mark_line().encode(
    x='Year:O',
    y='Revenue:Q',
    color='Industry Group:N'
).properties(
    title={
        'text':'Alberta Corporate Revenue by Sector',
        'subtitle':['Data: Alberta Treasury Board and Finance | Visualization: @jasonrwang',f"Last Updated: 2021-10-07"]
    }
).transform_filter(
    alt.datum['Industry Group'] != 'Oil and Gas Extraction'
)
og = alt.Chart(df).mark_line(size=3).encode(
    x='Year:O',
    y='Revenue:Q',
    color='Industry Group:N'
).transform_filter(
    alt.datum['Industry Group'] == 'Oil and Gas Extraction'
)
chart = base + og
# %%
# See visualization
chart
# %%
# Save visualization
chart.save('ABTax.png',scale_factor=2.0)
# %%
