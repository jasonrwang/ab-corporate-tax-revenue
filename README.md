# Alberta Corporate Tax Revenue

![Alberta Corporate Revenue by Sector (2002-2021)](ABTax.png)

Quick visualization of Alberta corporate tax revenue over time to look at the trends and context for  [Kim Siever's post](https://kimsiever.ca/2021/10/07/albertas-oil-gas-sector-generated-212m-in-corporate-tax-revenue-last-year/).

## Requirements

```{bash}
pandas
altair
```

Optionally, `ipykernel` to run within VS Code, or `juptyerlab` and `jupytext`.

## Try It

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/jasonrwang%2Fab-corporate-tax-revenue/main?urlpath=lab)

Click on the "launch binder" button above.

## Data Source

https://open.alberta.ca/opendata/net-corporate-income-tax-revenue